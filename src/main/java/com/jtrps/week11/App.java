package com.jtrps.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Plane jet = new Plane("Jet", "Ramjet Engine");
        jet.takeoff();
        jet.fly();
        jet.landing();

        Superman clark = new Superman("Clark");
        clark.eat();
        clark.sleep();
        clark.walk();
        clark.run();
        clark.swim();
        clark.takeoff();
        clark.fly();
        clark.landing();

        Human kaew = new Human("Kaew");
        kaew.eat();
        kaew.sleep();
        kaew.walk();
        kaew.run();
        kaew.swim();

        Bat bruce = new Bat("Bruce");
        bruce.eat();
        bruce.sleep();
        bruce.takeoff();
        bruce.fly();
        bruce.landing();

        Fish nemo = new Fish("Nemo");
        nemo.eat();
        nemo.sleep();
        nemo.swim();

        Crocodile mrZero = new Crocodile("Mr.Zero");
        mrZero.eat();
        mrZero.sleep();
        mrZero.crawl();
        mrZero.swim();

        Dog krypto = new Dog("Krypto");
        krypto.eat();
        krypto.sleep();
        krypto.walk();
        krypto.run();
        krypto.swim();

        Cat garfield = new Cat("Garfield");
        garfield.eat();
        garfield.sleep();
        garfield.walk();
        garfield.run();
        garfield.swim();

        Rat jerry = new Rat("Jerry");
        jerry.eat();
        jerry.sleep();
        jerry.walk();
        jerry.run();
        jerry.swim();

        Snake basilisk = new Snake("Basilisk");
        basilisk.eat();
        basilisk.sleep();
        basilisk.crawl();
        basilisk.swim();

        Flyable[] flyables = {bird1,jet,clark,bruce};
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        Walkable[] walkables = {bird1,clark,kaew,krypto,garfield,jerry};
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        Swimable[] swimables = {kaew,clark,nemo,mrZero,krypto,garfield,jerry,basilisk};
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }
        Crawlable[] crawables = {mrZero,basilisk};
        for (int i = 0; i < crawables.length; i++) {
            crawables[i].crawl();
        }
    }
}
